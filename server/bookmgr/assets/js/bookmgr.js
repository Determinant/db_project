var ajax_site = '/ajax'; /* just a local ajax */
var prompt = '';
var secured = false;
function send_request(cmd, term, on_success) {
    $.ajax({
        url: ajax_site + '?cmd=' + encodeURIComponent(cmd),
        type: 'GET',
        cache: false,
        dataType: 'json',
        async: true,
        success: on_success,
        timeout: 2000,
        error: function(a, b, c) {
            term.echo("failed while connecting, retrying...");
            console.log(b);
            setTimeout(function() {
                send_request(cmd, term, on_success);
            }, 2000);
        }
    });
}
function get_echo(cmd, term) {
    term.pause();
    send_request(cmd, term,
        function(resp) {
            var lines;
            lines = resp["stdout"].replace(/(Command not found: .*)\n/, '[[;#ff0000;]$1]\n').split("\n");
            secured = resp["secured"];
            prompt = lines[lines.length - 1];
            term.resume();
            term.echo(lines.slice(0, lines.length - 1).join("\n"));
            term.set_prompt(prompt);
            term.set_mask(secured);
        });
}

function get_greetings(term) {
    term.pause();
    send_request('', term,
        function(resp) {
            lines = resp["stdout"].split("\n");
            secured = resp["secured"];
            prompt = lines[lines.length - 1];
            term.resume();
            if (lines.length > 1)
                term.echo(lines.slice(0, lines.length - 1).join("\n"));
            else
                term.echo("[[;#E67E22;]Continued from the previous session.] To start a new session, type 'quit'. Type 'help' to list all commands.");
            term.set_prompt(prompt);
            term.set_mask(secured);
        });
}

$(function($, undefined) {
    var term = $('#term').terminal(function(command, term) {
        get_echo(command, term);
    }, {
        greetings: function(term) {
            return null;
        },
        name: 'bookmgr',
        height: '83%',
    prompt: ''});
    get_greetings(term);
});

$('#term').mousewheel(function(event, delta) {
    var media = this;
    if (delta > 0) {
        media.css('top', parseInt(media.css('top'))+20);
    } else {
        media.css('top', parseInt(media.css('top'))-20);
    } 
});
