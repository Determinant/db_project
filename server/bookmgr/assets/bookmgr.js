//var host = '59.78.57.151:8080/tomcat';
var host = 'vm:8080';
var ajax_site = 'http://' + host + '/ajax';
var prompt = '';
var secured = false;
function send_request(cmd, on_success) {
    $.ajax({
        url: ajax_site + '?cmd=' + encodeURIComponent(cmd),
        type: 'GET',
        cache: false,
        dataType: 'json',
        async: false,
        success: on_success,
        error: function(a, b, c) {
            console.log(c);
            setTimeout(function() {
                send_request(cmd, on_success);
            }, 2000);
        }
    });
}
function get_echo(cmd, term) {
    term.pause();
    send_request(cmd,
        function(resp) {
            var lines;
            lines = resp["stdout"].split("\n");
            secured = resp["secured"];
            prompt = lines[lines.length - 1];
            term.resume();
            term.echo(lines.slice(0, lines.length - 1).join("\n"));
            term.set_prompt(prompt);
            term.set_mask(secured);
        }
    );
}

function get_greetings(term) {
    term.pause();
    send_request('',
        function(resp) {
            lines = resp["stdout"].split("\n");
            secured = resp["secured"];
            prompt = lines[lines.length - 1];
            term.resume();
            console.log(lines);
            if (lines.length > 1)
                term.echo(lines.slice(0, lines.length - 1).join("\n"));
            term.set_prompt(prompt);
            term.set_mask(secured);
        }
    );
}

jQuery(function($, undefined) {
    var term = $('#term').terminal(function(command, term) {
        get_echo(command, term);
    }, {
        greetings: function(term) {
            return null;
        },
        name: 'bookmgr',
        height: '60%',
    prompt: ''});
    get_greetings(term);
});

