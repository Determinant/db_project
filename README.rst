Yet Another Book Ordering System
================================

:Author: Ted Yin (ted.sybil@gmail.com)

ER Diagram
----------
.. image:: ER.svg
    :align: center

How to Use
----------

- To build from the source code, run ``make build``
- To recreate the database tables, run ``make install``
- After build, run ``make run`` to fire up the main program
- The default book manager account has username "root" and password "123456"
  (see ``Installer.java``). Some operations such as adding new books or
  generating statistics can only be done as root.
- Upon the entrance of each page, the available commands are printed
- Type ``help`` command to print the full help message (note that
  different help messages are printed in different pages, e.g. try to
  ``login`` and print ``help`` again)
- A typical example: type ``register guest`` to register a new account
  and then ``login guest`` to log in, after logging in, type ``logout``
  and ``login root`` to login again as root, finally ``newbook`` to add a
  book.

Interesting Features
--------------------

Page-based Design
*****************
The program is comprised of several pages just like a web application.
The implementation of any specific pages inherits from a base class
``Navigator``, and a unified ``interface`` named ``CmdCallback`` is used
to store the callbacks which are triggered by user input. This design
benefits from OOP since a page can inherit commands from another. For
example, the top-level class supports only ``help`` and ``quit`` commands
which are shared to all other classes. And ``RootNavi`` inherits all
commands from ``UserNavi`` and add a few additional ones to permit the
book manager to do all what a normal user can do as well as some
privileged operations.

Robustness
**********
The command and date inputs are both verified. When a command is typed with
wrong arguments, a help message is printed. Wrong data format is also rejected
before doing actual business operations.

Fancy output
************

Help messages and tabular output are both fancy-formatted. A function
``Utils.printTabular`` handles all these outputs. Notice that long text
output that exceeds the expecting column width is automatic broken into
several contiguous lines. That being said, all line breaks in help
messages and query output are automatic generated (the author only has to
invoke ``addTriggerEasy`` (in ``CUIView.java``) to provide bare text).

For example:

::

    +--------------------------------------------------------------------------------------------------------------+
    |                                                     Feedback                                                 |
    +-------------------+--------------+-------+-------------------------+-----------------+-----------------------+
    |     Full Name     |   Username   | Score |         Comment         | Avg. Usefulness |          Time         |
    +-------------------+--------------+-------+-------------------------+-----------------+-----------------------+
    | Goblin            | goblin       | 5     | In no impression assist | 0.0             | 2014-12-09 13:14:13.0 |
    |                   |              |       | ance contrasted. Manner |                 |                       |
    |                   |              |       | s she wishing justice h |                 |                       |
    |                   |              |       | astily new anxious. At  |                 |                       |
    |                   |              |       | discovery discourse dep |                 |                       |
    |                   |              |       | arture objection we. Fe |                 |                       |
    |                   |              |       | w extensive add delight |                 |                       |
    |                   |              |       | ed tolerably sincerity  |                 |                       |
    |                   |              |       | her. Law ought him leas |                 |                       |
    |                   |              |       | t enjoy decay one quick |                 |                       |
    |                   |              |       |  court. Expect warmly i |                 |                       |
    |                   |              |       | ts tended garden him es |                 |                       |
    |                   |              |       | teem had remove off. Ef |                 |                       |
    |                   |              |       | fects dearest staying n |                 |                       |
    |                   |              |       | ow sixteen nor improve. |                 |                       |
    | Ted               | ymf          | 0     | Nothing valuable.       | 0.0             | 2014-12-09 13:08:40.0 |
    | N/A               | root         | 10    | This comment contains o | 0.0             | 2014-12-09 13:07:55.0 |
    |                   |              |       | nly nonsense nonsense n |                 |                       |
    |                   |              |       | onsense. Thus it is not |                 |                       |
    |                   |              |       |  reliable not reliable  |                 |                       |
    |                   |              |       | not reliable. The quick |                 |                       |
    |                   |              |       |  brown fox jumps over a |                 |                       |
    |                   |              |       |  lazy dog. The quick br |                 |                       |
    |                   |              |       | own fox jumps over a la |                 |                       |
    |                   |              |       | zy dog. What else, the  |                 |                       |
    |                   |              |       | author of the program w |                 |                       |
    |                   |              |       | ants a girlfriend. He i |                 |                       |
    |                   |              |       | s insane. Forget it. Th |                 |                       |
    |                   |              |       | is is the end of the co |                 |                       |
    |                   |              |       | mment.                  |                 |                       |
    +-------------------+--------------+-------+-------------------------+-----------------+-----------------------+
