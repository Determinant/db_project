package bookmgr;
class UserException extends Exception {
    public enum TypeId {
        BOOK_NOT_EXIST,
        BOOK_INSUFFIICENT_COPIES,
        FB_ALREADY_EXIST,
        FB_NOT_EXIST,
        USER_NOT_EXIST,
        RATE_ALREADY_EXIST,
        RATE_SELF_FORBIDDEN,
        TRUST_NOT_EXIST,
        MAINLOOP_QUIT
    };
    public TypeId type;

    public UserException(TypeId type) {
        super();
        this.type = type;
    }
}
