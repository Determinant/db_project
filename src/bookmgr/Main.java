package bookmgr;
class Main {

    public static void main(String[] args) {
        try
        {
            Connector con = new Connector();
            CUIView cui = new CUIView(con.conn, true);
            cui.mainLoop();
            con.closeConnection();
        }
        catch (InternalError e)
        {
            e.printStackTrace();
        }
    }
}
