package bookmgr;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class AjaxSessionListener implements HttpSessionListener {
    public void sessionCreated(HttpSessionEvent e) {
        try {
            CUIView.CUIThread thread = new CUIView.CUIThread();
            e.getSession().setAttribute("thread", thread);
        } catch (InternalError exc) {
            exc.printStackTrace();
        }
    }

    public void sessionDestroyed(HttpSessionEvent e) {
        CUIView.CUIThread thread = (CUIView.CUIThread)e.getSession().getAttribute("thread");
        if (thread != null)
        {
            CUIView.UserConsole console = thread.view.getConsole();
            console.kill();
            console.hasInput.signal();
        }
    }
}
