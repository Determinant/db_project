package bookmgr;
public class InternalError extends Exception {
    public InternalError(String msg) {
        super(msg);
    }
}
