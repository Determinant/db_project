package bookmgr;
import java.sql.Connection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Calendar;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

abstract class Navigator {
    abstract public void greetingMsg();
//    abstract public Navigator navigate(String cmd, String[] args) throws UserException, InternalError;
    static protected interface CmdCallback {
        public String getHelp();
        public String getUsage();
        public Navigator trigger(String args[]) throws UserException, InternalError;
    }

    private HashMap<String, CmdCallback> str2callback = new HashMap<String, CmdCallback>();
    protected Presenter presenter;
    protected CUIView.UserConsole console;
    public Navigator(CUIView.UserConsole console, Presenter presenter)
        throws UserException, InternalError {
        this.presenter = presenter;
        this.console = console;
        addTriggerEasy("help", "show help information", "help [<command> ...]");
        addTriggerEasy("quit", "terminate the program", "quit");
    }

    public String getPrompt() {
        return "> ";
    }

    protected boolean checkArgNum(String cmd, String[] args, int num) {
        if (args.length != num)
        {
            console.printf("%s: wrong number of arguments%n", cmd);
            CmdCallback cb = getCallback(cmd);
            if (cb != null)
                console.printf("Usage: %s%n", cb.getUsage());
            return false;
        }
        return true;
    }

    protected void printUsers(LinkedList<Presenter.User> users) {
        String[] fullNameCol = new String[users.size()];
        String[] usernameCol = new String[users.size()];
        int i = 0;
        for (Presenter.User u: users)
        {
            fullNameCol[i] = u.fullName;
            usernameCol[i] = u.username;
            i++;
        }
        Utils.printTabular(console, new String[] {"    Full Name    ", "  Username  "},
                            Utils.SHOW_HEADER | Utils.SHOW_EMPTY, '+', '-', '|',
                            fullNameCol, usernameCol);
    }

    protected void printBooks(LinkedList<Presenter.Book> books) {
        String[] titleCol = new String[books.size()];
        String[] isbnCol = new String[books.size()];
        String[] authorCol = new String[books.size()];
        String[] pubCol = new String[books.size()];
        int i = 0;
        for (Presenter.Book b: books)
        {
            titleCol[i] = b.title;
            isbnCol[i] = b.isbn;
            authorCol[i] = b.formattedAuthors();
            pubCol[i] = b.publisher;
            i++;
        }

        Utils.printTabular(console, new String[] {"        Title       ", "       ISBN         ", "    Author(s)   ", "    Publisher    "},
                Utils.SHOW_HEADER | Utils.SHOW_EMPTY, '+', '-', '|',
                titleCol, isbnCol, authorCol, pubCol);
    }

    protected String[] parseStringAndCheck(String raw, int lower, int upper) {
        String[] tokens = raw.split("\\s*,\\s*");
        if (tokens.length < 1 || tokens[0].length() == 0)
        {
            console.printf("no authors specified%n");
            return null;
        }
        for (int i = 0; i < tokens.length; i++)
        {
            tokens[i] = tokens[i].trim();
            if (tokens[i].length() == 0)
            {
                console.printf("empty name encountered%n");
                return null;
            }
        }
        return tokens;
    }

    protected Float readFloatAndCheck(String name, float lower, float upper, float df, String remark) throws UserException {
        String str = console.readLine("Please enter %s (%.3f ~ %.3f%s): ",
                                                        name, lower, upper, remark);
        float num;
        if (str.length() == 0 && df >= lower && df <= upper)
            return df; /* use default number */
        try {
            num = Float.parseFloat(str);
        } catch (NumberFormatException e) {
            console.printf("not a valid integral number%n");
            return null;
        }
        if (num < lower)
        {
            console.printf("%s should not be less than %.3f%n",
                                name, lower);
            return null;
        }
        if (num > upper)
        {
            console.printf("%s should not exceed %.3f%n",
                                name, upper);
            return null;
        }
        return num;
    }

    protected Integer parseIntAndCheck(String name, String str, int lower, int upper, int df) {
        int num;
        if (str.length() == 0 && df >= lower && df <= upper)
            return df; /* use default number */
        try {
            num = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            console.printf("not a valid integral number%n");
            return null;
        }
        if (num < lower)
        {
            console.printf("%s should not be less than %d%n",
                    name, lower);
            return null;
        }
        if (num > upper)
        {
            console.printf("%s should not exceed %d%n",
                    name, upper);
            return null;
        }
        return num;
    }

    protected Integer readIntAndCheck(String name, int lower, int upper, int df, String remark) throws UserException {
        return parseIntAndCheck(name, console.readLine("Please enter %s (%d ~ %d%s): ",
                                                        name, lower, upper, remark),
                                lower, upper, df);
    }

    protected Boolean readConjCheck() throws UserException {
        String str = console.readLine("and (default)/or?: ");
        if (str.equals("and"))
            return true;
        else if (str.equals("or"))
            return false;
        else if (str.length() == 0)
            return true;
        else
        {
            console.printf("please type a conjunction 'and' or 'or'%n");
            return null;
        }
    }

    protected String readStringAndCheck(String name, int lower, int upper, String remark) throws UserException {
        String str = console.readLine("Please enter %s (%d ~ %d characters%s): ",
                                                        name, lower, upper, remark);
        if (str.length() < lower)
        {
            console.printf("the length of %s should not be less than %d%n",
                                name, lower);
            return null;
        }
        if (str.length() > upper)
        {
            console.printf("the length of %s should not exceed %d%n",
                                name, upper);
            return null;
        }
        return str;
    }

    protected void addTrigger(String cmd, CmdCallback cb) {
        str2callback.put(cmd, cb);
    }

    protected void addTriggerEasy(final String cmd, final String helpMsg, final String usageMsg) throws UserException, InternalError {
        try {
            final Method m = getClass().getMethod(cmd, String[].class);
            final Object obj = this;
            addTrigger(cmd, new CmdCallback() {
                public String getHelp() { return helpMsg; }
                public String getUsage() { return usageMsg; }
                public Navigator trigger(String[] args) throws UserException, InternalError {
                    try {
                        return (Navigator)m.invoke(obj, (Object)args);
                    } catch (InvocationTargetException e) {
                        Throwable c = e.getCause();
                        if (c instanceof UserException)
                            throw (UserException)c;
                        else if (c instanceof InternalError)
                            throw (InternalError)c;
                        else
                        {
                            c.printStackTrace();
                            throw new Error("error while invoking callback " + cmd);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        throw new Error("error while accessing callback " + cmd);
                    }
                }});
        } catch (NoSuchMethodException e) {
            throw new InternalError("callback not found");
        }
    }

    public Navigator navigate(String cmd, String[] args) throws UserException, InternalError {
        CmdCallback cb = str2callback.get(cmd);
        if (cb == null)
            return null; /* nothing we can do about this */
        return cb.trigger(args);
    }

    protected CmdCallback getCallback(String cmd) {
        CmdCallback cb = str2callback.get(cmd);
        if (cb == null)
            console.printf("Command not found: %s%n", cmd);
        return cb;
    }

    public Navigator quit(String[] args) throws UserException, InternalError {
        /* exit the main loop */
        throw new UserException(UserException.TypeId.MAINLOOP_QUIT);
    }

    public Navigator help(String[] args) throws UserException, InternalError {
        int length = args.length == 0 ? str2callback.size() : args.length;
        String[] cmdCol = new String[length];
        String[] helpCol = new String[length];
        String[] usageCol = new String[length];

        if (args.length == 0) /* print all help msg */
        {
            int i = 0;
            for (Map.Entry<String, CmdCallback> entry: str2callback.entrySet())
            {
                cmdCol[i] = entry.getKey();
                helpCol[i] = entry.getValue().getHelp();
                usageCol[i] = entry.getValue().getUsage();
                i++;
            }
        }
        else 
        {
            for (int i = 0; i < args.length; i++)
            {
                CmdCallback cb = getCallback(args[i]);
                if (cb == null)
                    return this;
                cmdCol[i] = args[i];
                helpCol[i] = cb.getHelp();
                usageCol[i] = cb.getUsage();
            }
        }
        Utils.printTabular(console, new String[] {"Command      ",
            "Description" + Utils.charLine(20, ' '),
            "Usage" + Utils.charLine(19, ' ')},
            Utils.SHOW_HEADER | Utils.SHOW_EMPTY, ' ', ' ', ' ',
            cmdCol, helpCol, usageCol);
        return this;
    }
}

class MainNavi extends Navigator {

    public MainNavi(CUIView.UserConsole console, Presenter p) throws UserException, InternalError {
        super(console, p);
        addTriggerEasy("login", "log into the system", "login <username>");
        addTriggerEasy("register", "create a new account", "register <username>");
    }

    public String getPrompt() {
        return "main> ";
    }

    public void greetingMsg() {
        Utils.printTabular(console, new String[] {"         Welcome to Yet Another Book Ordering System        "},
                            Utils.SHOW_HEADER, '*', '=', '|',
                          (Object)new String[] {"type 'login <username>' to log in, 'register <username>' to create a new account, use 'help' to help yourself, and type 'quit' or press Ctrl-D to terminate the program"});
    }

    public Navigator login(String[] args) throws UserException, InternalError {
        if (!checkArgNum("login", args, 1))
            return this;
        Presenter.User user = presenter.new User(args[0]);
        if (user.login(console.readPassword("Please enter your password: ")))
        {
            console.printf("Access granted%n");
            if (user.privilege == Presenter.User.PRIV_ADMIN)
                return new RootNavi(console, presenter, user);
            else
                return new UserNavi(console, presenter, user);
        }
        else
        {
            console.printf("Incorrect username/password%n");
            return this;
        }
    }

    public Navigator register(String[] args) throws UserException, InternalError {
        if (!checkArgNum("register", args, 1))
            return this;
        /* check the validity of username */
        if (args[0].length() > Presenter.User.MAX_USERNAME_LENGTH)
        {
            console.printf("the length of user name should not exceed %d%n",
                            Presenter.User.MAX_USERNAME_LENGTH);
            return this;
        }
        Presenter.User user = presenter.new User(args[0]);
        String fullName = readStringAndCheck("your full name", 1,
                                Presenter.User.MAX_FULLNAME_LENGTH, "");
        if (fullName == null)
            return this;
        String phoneNumber = readStringAndCheck("your phone number", 0,
                                Presenter.User.MAX_PHONE_NUMBER_LENGTH,
                                ", optional");
        if (phoneNumber == null)
            return this;
        String address = readStringAndCheck("your address", 0,
                                Presenter.User.MAX_ADDRESS_LENGTH,
                                ", optional");
        if (address == null)
            return this;
        user.fullName = fullName;
        user.phoneNumber = phoneNumber;
        user.address = address;
        String password = console.readPassword("Please enter your password: ");
        String cpassword = console.readPassword("Please confirm your password: ");
        if (!password.equals(cpassword))
        {
            console.printf("Mismatched passwords%n");
            return this;
        }
        if (user.register(password))
            console.printf("Successfully registered%n");
        else
            console.printf("Username already exists, please pick another one%n");
        return this;
    }
}

class UserNavi extends Navigator {
    protected Presenter.User user;
    public UserNavi(CUIView.UserConsole console, Presenter p, Presenter.User user)
        throws UserException, InternalError {
        super(console, p);
        this.user = user;
        addTriggerEasy("logout", "logout the current account", "logout");
        addTriggerEasy("order", "order a new book", "order");
        addTriggerEasy("feedback", "give feedback to a book", "feedback");
        addTriggerEasy("rate", "rate others' feedback", "rate");
        addTriggerEasy("trust", "trust a user", "trust <username>");
        addTriggerEasy("untrust", "untrust a user", "untrust <username>");
        addTriggerEasy("forget", "forget about a trusted/untrusted user", "forget <username>");
        addTriggerEasy("useful", "show top n most useful feedback", "useful n");
        addTriggerEasy("degsep", "determine the degree of separation of two users", "degsep");
        addTriggerEasy("find", "find a book", "find");
        addTriggerEasy("info", "show detailed information of a book", "info <isbn>");
    }

    public String getPrompt() {
        return String.format("%s> ", user.username);
    }

    protected void printUserInfo() {
        Utils.printTabular(console, new String[] {"           ", "              "},
                            0, '*', '-', '|',
                            new String[] {"Full Name", "Username", "Phone", "Address"},
                            new String[] {user.username, user.fullName, user.phoneNumber, user.address});
    }

    public void greetingMsg() {
        Utils.printTabular(console, new String[] {String.format("       Hi, %s (%s)        ", user.fullName, user.username)},
                            Utils.SHOW_HEADER, '*', '*', '|',
                            (Object)new String[] {});

        printUserInfo();
    }

    public Navigator info(String[] args) throws UserException, InternalError {
        if (!checkArgNum("info", args, 1))
            return this;
        try {
            Presenter.Book book = user.getBook(args[0]);
            Utils.printTabular(console, new String[] {Utils.charLine(14, ' '), Utils.charLine(50, ' ')},
                                0, '+', '-', '|',
                                new String[] {"Title", "ISBN", "Authors",
                                            "Publisher", "Format", "Year of Pub.",
                                            "Subject", "Num. of Copies", "Keywords"},
                                new String[] {book.title, book.isbn, book.formattedAuthors(),
                                            book.publisher, book.hardcover ? "hardcover" : "paperback",
                                            Integer.toString(book.yearOfPub), book.subject,
                                            Integer.toString(book.numOfCopies),
                                            book.keywords});
            LinkedList<Presenter.haveFeedback> fbs = book.getFeedback();
            String[] fullnameCol = new String[fbs.size()];
            String[] usernameCol = new String[fbs.size()];
            String[] scoreCol = new String[fbs.size()];
            String[] remarkCol = new String[fbs.size()];
            String[] avgCol = new String[fbs.size()];
            String[] timeCol = new String[fbs.size()];
            int i = 0;
            for (Presenter.haveFeedback fb: fbs)
            {
                fullnameCol[i] = fb.fullName;
                usernameCol[i] = fb.username;
                scoreCol[i] = Integer.toString(fb.score);
                remarkCol[i] = fb.remark;
                avgCol[i] = Float.toString(fb.average);
                timeCol[i] = fb.time.toString();
                i++;
            }
            console.printf("+--------------------------------------------------------------------------------------------------------------+%n");
            console.printf("|                                                     Feedback                                                 |%n");
            Utils.printTabular(console, new String[] {"    Full Name    ", "  Username  ",
                                            "Score", "        Comment        ",
                                            "Avg. Usefulness",  "         Time        "},
                Utils.SHOW_HEADER | Utils.SHOW_EMPTY, '+', '-', '|',
                fullnameCol, usernameCol, scoreCol, remarkCol, avgCol, timeCol);

        } catch (UserException e) {
            if (e.type == UserException.TypeId.BOOK_NOT_EXIST)
                console.printf("No such book\n");
            return this;
        }
        return this;
    }

    public Navigator find(String[] args) throws UserException, InternalError {
        String author = readStringAndCheck("an author name", 0,
                                Presenter.Book.MAX_AUTHOR_LENGTH, ", optional, press enter to indicate an arbitrary one");
        if (author == null) return this;
        Boolean conj1 = readConjCheck();
        if (conj1 == null) return this;
        String pub = readStringAndCheck("a publisher name", 0,
                                Presenter.Book.MAX_PUBLISHER_LENGTH, ", optional");
        if (pub == null) return this;
        Boolean conj2 = readConjCheck();
        if (conj2 == null) return this;
        String title = readStringAndCheck("a title word", 0,
                                Presenter.Book.MAX_TITLE_LENGTH, ", optional");
        if (title == null) return this;
        Boolean conj3 = readConjCheck();
        if (conj3 == null) return this;
        String subject = readStringAndCheck("the subject", 0,
                                Presenter.Book.MAX_SUBJECT_LENGTH, ", optional");
        Integer modeNum = readIntAndCheck("the order mode", 0, 2, 0, ", 0 by year (default), 1 by the avg score of feedback, 2 by the avg score of trusted user feedback");
        if (modeNum == null) return this;
        Presenter.OrderMode mode = null;
        switch (modeNum)
        {
            case 0: mode = Presenter.OrderMode.BY_YEAR; break;
            case 1: mode = Presenter.OrderMode.BY_AVG_SCORE; break;
            case 2: mode = Presenter.OrderMode.BY_AVG_TRUSTED_SCORE; break;
        }
        assert mode != null;
        LinkedList<Presenter.Book> books = user.findBooks(author, pub, title, subject, conj1, conj2, conj3, mode);
        printBooks(books);
        return this;
    }

    public Navigator degsep(String[] args) throws UserException, InternalError {
        String author1 = readStringAndCheck("an author name", 1,
                                Presenter.Book.MAX_AUTHOR_LENGTH, "");
        if (author1 == null) return this;
        String author2 = readStringAndCheck("another author name", 1,
                                Presenter.Book.MAX_AUTHOR_LENGTH, "");
        if (author2 == null) return this;
        if (user.checkOneDegSep(author1, author2))
            console.printf("One-degree away%n");
        else if (user.checkTwoDegSep(author1, author2))
            console.printf("Two-degree away%n");
        else
            console.printf("Neither%n");
        return this;
    }

    public Navigator useful(String[] args) throws UserException, InternalError {
        if (!checkArgNum("useful", args, 1))
            return this;
        Integer limit = parseIntAndCheck("limit", args[0], 1, 100, -1);
        if (limit == null) return this;
        LinkedList<Presenter.haveFeedback> fbs = (presenter.new haveFeedback()).usefulFeedback(limit); 
        String[] fullnameCol = new String[fbs.size()];
        String[] usernameCol = new String[fbs.size()];
        String[] titleCol = new String[fbs.size()];
        String[] isbnCol = new String[fbs.size()];
        String[] scoreCol = new String[fbs.size()];
        String[] remarkCol = new String[fbs.size()];
        String[] avgCol = new String[fbs.size()];
        int i = 0;
        for (Presenter.haveFeedback fb: fbs)
        {
            fullnameCol[i] = fb.fullName;
            usernameCol[i] = fb.username;
            titleCol[i] = fb.title;
            isbnCol[i] = fb.isbn;
            scoreCol[i] = Integer.toString(fb.score);
            remarkCol[i] = fb.remark;
            avgCol[i] = Float.toString(fb.average);
            i++;
        }
        Utils.printTabular(console, new String[] {"    Full Name    ", "  Username  ", "      Book Title      ",
                        "      Book ISBN      ", "  Score  ", "        Comment        ", "Avg. Usefulness"},
                        Utils.SHOW_HEADER | Utils.SHOW_EMPTY, '+', '-', '|',
                        fullnameCol, usernameCol, titleCol, isbnCol, scoreCol, remarkCol, avgCol);
        return this;
    }

    public Navigator forget(String[] args) throws UserException, InternalError {
        checkArgNum("forget", args, 1);
        try {
            user.removePref(args[0]);
        } catch (UserException e) {
            switch (e.type)
            {
                case USER_NOT_EXIST:
                    console.printf("No such user%n");
                    break;
                case TRUST_NOT_EXIST:
                    console.printf("Nothing to forget about%n");
                default: assert false;
            }
            return this;
        }
        return this;
    }

    public Navigator trust(String[] args) throws UserException, InternalError {
        checkArgNum("trust", args, 1);
        try {
            user.addPref(args[0], true);
            console.printf("You now trust %s%n", args[0]);
        } catch (UserException e) {
            switch (e.type)
            {
                case USER_NOT_EXIST:
                    console.printf("No such user%n");
                    break;
                default: assert false;
            }
            return this;
        }
        return this;
    }

    public Navigator untrust(String[] args) throws UserException, InternalError {
        checkArgNum("untrust", args, 1);
        try {
            user.addPref(args[0], false);
            console.printf("You now do not trust %s%n", args[0]);
        } catch (UserException e) {
            switch (e.type)
            {
                case USER_NOT_EXIST:
                    console.printf("No such user%n");
                    break;
                default: assert false;
            }
            return this;
        }
        return this;
    }

    public Navigator rate(String[] args) throws UserException, InternalError {
        String username = readStringAndCheck("the username", 1,
                                Presenter.User.MAX_USERNAME_LENGTH, "");
        if (username == null) return this;
        String isbn = readStringAndCheck("the ISBN", 1,
                                Presenter.Book.MAX_ISBN_LENGTH, "");
        if (isbn == null) return this;
        Integer rate = readIntAndCheck("the usefulness", 0,
                                Presenter.haveFeedback.MAX_USEFULNESS, -1, "");
        if (rate == null) return this;
        Presenter.haveFeedback fb = presenter.new haveFeedback(); 
        try {
            fb.rate(user.uid, username, isbn, rate);
        } catch (UserException e) {
            switch (e.type)
            {
                case FB_NOT_EXIST:
                    console.printf("No such feedback%n");
                    break;
                case BOOK_NOT_EXIST:
                    console.printf("No such book%n");
                    break;
                case USER_NOT_EXIST:
                    console.printf("No such user%n");
                    break;
                case RATE_ALREADY_EXIST:
                    console.printf("Your have rated this feedback%n");
                    break;
                case RATE_SELF_FORBIDDEN:
                    console.printf("You cannot rate your own feedback%n");
                    break;
                default: assert false;
            }
            return this;
        }
        return this;
    }

    public Navigator feedback(String[] args) throws UserException, InternalError {
        String isbn = readStringAndCheck("the ISBN", 1,
                                Presenter.Book.MAX_ISBN_LENGTH, "");
        if (isbn == null) return this;
        String remark = readStringAndCheck("your remark", 0,
                                Presenter.haveFeedback.MAX_REMARK_LENGTH, ", optional");
        if (remark == null) return this;
        Integer score = readIntAndCheck("your score", 0,
                                Presenter.haveFeedback.MAX_SCORE, -1, "");
        if (score == null) return this;
        java.util.Date date = new java.util.Date(System.currentTimeMillis());
        Presenter.haveFeedback fb = presenter.new haveFeedback(); 
        fb.remark = remark;
        fb.score = score;
        try {
            fb.add(user.uid, isbn, date);
        } catch (UserException e) {
            switch (e.type)
            {
                case FB_ALREADY_EXIST:
                    console.printf("Your have already given feedback to this book%n");
                    break;
                case BOOK_NOT_EXIST:
                    console.printf("No such book%n");
                    break;
                default: assert false;
            }
            return this;
        }
        return this;
    }

    public Navigator order(String[] args) throws UserException, InternalError {
        String isbn = readStringAndCheck("the ISBN", 1,
                                Presenter.Book.MAX_ISBN_LENGTH, "");
        if (isbn == null) return this;
        Integer numOfCopies = readIntAndCheck("the number of copies", 1,
                                Presenter.Book.MAX_COPIES,
                                Presenter.Book.DEFAULT_BOOK_NUM_OF_COPIES,
                                ", press enter to use default value: " +
                                Presenter.Book.DEFAULT_BOOK_NUM_OF_COPIES);
        if (numOfCopies == null) return this;
        Presenter.Book book = presenter.new Book();
        book.isbn = isbn;
        java.util.Date date = new java.util.Date(System.currentTimeMillis());
        try {
            book.order(user.uid, numOfCopies, date);
        } catch (UserException e) {
            switch (e.type)
            {
                case BOOK_NOT_EXIST:
                    console.printf("The specified book does not exist%n");
                    break;
                case BOOK_INSUFFIICENT_COPIES:
                    console.printf("Insufficient copies%n");
                    break;
                default: assert false;
            }
            return this;
        }
        console.printf("ordered %d book(s)%n", numOfCopies);
        LinkedList<Presenter.Book> recommded = user.recommend(book.bid);
        console.printf("+----------------------------------------------------------------+%n");
        console.printf("|           Customers Who Bought This Item Also Bought           |%n");
        printBooks(recommded);
        return this;
    }

    public Navigator logout(String[] args) throws UserException, InternalError {
        user.logout();
        return new MainNavi(console, presenter);
    }
}

class RootNavi extends UserNavi {
    public RootNavi(CUIView.UserConsole console, Presenter p, Presenter.User user)
        throws UserException, InternalError {
        super(console, p, user);
        addTriggerEasy("newbook", "add a new book", "newbook");
        addTriggerEasy("morebooks", "provide more copies of a book", "morebooks");
        addTriggerEasy("genstat", "generate statitcs (n popular)", "genstat n");
        addTriggerEasy("bestusers", "find out the top n users", "bestusers n");
    }
    public void greetingMsg() {
        Utils.printTabular(console, new String[] {"    Welcome, my lord.       "},
                            Utils.SHOW_HEADER, '*', '*', '|',
                            (Object)new String[] {});
        printUserInfo();
    }

    public Navigator genstat(String[] args) throws UserException, InternalError {
        if (!checkArgNum("genstat", args, 1))
            return this;
        Integer limit = parseIntAndCheck("limit", args[0], 1, 100, -1);
        if (limit == null) return this;
        int defaultYear = Calendar.getInstance().get(Calendar.YEAR);
        Integer year = readIntAndCheck("the year of semester", 0,
                                Presenter.Book.MAX_YEAR, defaultYear,
                                String.format(", default: %d", defaultYear));
        if (year == null) return this;
        Integer startMonth = readIntAndCheck("the first month of the semester", 1,
                                12, 1,  ", default: 1");
        if (startMonth == null) return this;
        Integer endMonth = readIntAndCheck("the last month of the semester", 1,
                                12, 12,  ", default: 12");
        if (endMonth == null) return this;
        LinkedList<Presenter.Book> books = user.getPopularBooks(year, startMonth, endMonth, limit);
        LinkedList<String> authors = user.getPopularAuthors(year, startMonth, endMonth, limit);
        LinkedList<String> pubs = user.getPopularPublishers(year, startMonth, endMonth, limit);
        console.printf("+-----------------------------------------------------------+%n");
        console.printf("|                 The Most Popular Book(s)                  |%n");
        String[] titleCol = new String[books.size()];
        String[] isbnCol = new String[books.size()];
        String[] soldCol = new String[books.size()];
        int i = 0;
        for (Presenter.Book b: books)
        {
            titleCol[i] = b.title;
            isbnCol[i] = b.isbn;
            soldCol[i] = Integer.toString(b.numOfCopies);
            i++;
        }
        Utils.printTabular(console, new String[] {"        Title       ", "       ISBN         ", "Sold Copies"},
                            Utils.SHOW_HEADER | Utils.SHOW_EMPTY, '+', '-', '|',
                            titleCol, isbnCol, soldCol);
                            
        String[] authorCol = new String[authors.size()];
        i = 0;
        for (String name: authors)
            authorCol[i++] = name;
        Utils.printTabular(console, new String[] {"The Most Popular Author(s)"},
                            Utils.SHOW_HEADER | Utils.SHOW_EMPTY, '+', '-', '|',
                            (Object)authorCol);
        String[] pubCol = new String[pubs.size()];
        i = 0;
        for (String name: pubs)
            pubCol[i++] = name;
        Utils.printTabular(console, new String[] {"The Most Popular Publisher(s)"},
                            Utils.SHOW_HEADER | Utils.SHOW_EMPTY, '+', '-', '|',
                            (Object)pubCol);
        return this;
    }

    public Navigator bestusers(String[] args) throws UserException, InternalError {
        if (!checkArgNum("bestusers", args, 1))
            return this;
        Integer limit = parseIntAndCheck("limit", args[0], 1, 100, -1);
        if (limit == null) return this;
        LinkedList<Presenter.User> trustedUsers = user.getTrustedUsers(limit);
        LinkedList<Presenter.User> usefulUsers = user.getUsefulUsers(limit);
        console.printf("+----------------------------------+%n");
        console.printf("|      The Most Trusted Users      |%n");
        printUsers(trustedUsers);
        console.printf("%n");
        console.printf("+----------------------------------+%n");
        console.printf("|      The Most Useful Users       |%n");
        printUsers(usefulUsers);
        return this;
    }

    public Navigator morebooks(String[] args) throws UserException, InternalError {
        String isbn = readStringAndCheck("the ISBN", 1,
                                Presenter.Book.MAX_ISBN_LENGTH, "");
        if (isbn == null) return this;
        Integer numOfCopies = readIntAndCheck("the number of copies", 1,
                                Presenter.Book.MAX_COPIES,
                                Presenter.Book.DEFAULT_BOOK_NUM_OF_COPIES,
                                ", press enter to use default value: " +
                                Presenter.Book.DEFAULT_BOOK_NUM_OF_COPIES);
        if (numOfCopies == null) return this;
        Presenter.Book book = presenter.new Book();
        book.isbn = isbn;
        if (!book.morebooks(numOfCopies))
            console.printf("The specified book does not exist%n");
        else
            console.printf("Add %d more book(s)%n", numOfCopies);
        return this;
    }

    public Navigator newbook(String[] args) throws UserException, InternalError {
        String title = readStringAndCheck("the title", 1,
                                Presenter.Book.MAX_TITLE_LENGTH, "");
        if (title == null) return this;
        String rawAuthors = readStringAndCheck("the author(s)", 1,
                                Presenter.Book.MAX_AUTHORS_LENGTH,
                                "seperated by ',' with optional whitespaces");
        if (rawAuthors == null) return this;
        String[] authors = parseStringAndCheck(rawAuthors,
                                1, Presenter.Book.MAX_AUTHOR_LENGTH);
        if (authors == null) return this;
        String pub = readStringAndCheck("the publisher", 1,
                                Presenter.Book.MAX_PUBLISHER_LENGTH, "");
        if (pub == null) return this;
        String subject = readStringAndCheck("the subject", 0,
                                Presenter.Book.MAX_SUBJECT_LENGTH, ", optional");
        if (subject == null) return this;
        String isbn = readStringAndCheck("the ISBN", 1,
                                Presenter.Book.MAX_ISBN_LENGTH, "");
        if (isbn == null) return this;
        String keywords = readStringAndCheck("the keywords", 0,
                                Presenter.Book.MAX_KEYWORDS_LENGTH, ", optional");
        if (keywords == null) return this;
        Integer numOfCopies = readIntAndCheck("the number of copies", 1,
                                Presenter.Book.MAX_COPIES,
                                Presenter.Book.DEFAULT_BOOK_NUM_OF_COPIES,
                                ", press enter to use default value: " +
                                Presenter.Book.DEFAULT_BOOK_NUM_OF_COPIES);
        if (numOfCopies == null) return this;
        Integer yearOfPub = readIntAndCheck("the year of publication", 0,
                                Presenter.Book.MAX_YEAR,
                                Presenter.Book.DEFAULT_BOOK_YEAR,
                                ", press enter to use default value: " +
                                Presenter.Book.DEFAULT_BOOK_YEAR);
        if (yearOfPub == null) return this;
        Float price = readFloatAndCheck("the price", 0.0f,
                                Presenter.Book.MAX_PRICE, -1.0f, "");
        if (price == null) return this;
        Integer hardCover = readIntAndCheck("the format", 0, 1,
                                Presenter.Book.DEFAULT_BOOK_HARDCOVER,
                                ", 0 for paperback, 1 for hardcover, press enter to use default value: " +
                                Presenter.Book.DEFAULT_BOOK_HARDCOVER);
        if (hardCover == null) return this;
        /*
        String title = "a";
        String pub = "b";
        String subject = "subject";
        String keywords = "k1, k2";
        String isbn = "222";
        String[] authors = parseStringAndCheck("aa, bb, cc",
                                1, Presenter.Book.MAX_AUTHOR_LENGTH);
        int numOfCopies = 2;
        int yearOfPub = 3;
        float price = 0;
        int hardCover = 1;
        */
        Presenter.Book book = presenter.new Book();
        book.title = title;
        book.publisher = pub;
        book.subject = subject;
        book.keywords = keywords;
        book.isbn = isbn;
        book.authors = authors;
        book.numOfCopies = numOfCopies;
        book.yearOfPub = yearOfPub;
        book.price = price;
        book.hardcover = hardCover == 1;
        if (!book.newbook())
            console.printf("The book already exists%n");
        return this;
    }
}

class CUIView {
    private Presenter presenter;
    public CUIView(Connection conn, boolean realConsole) {
        this.presenter = new Presenter(conn);
        this.console = new UserConsole(realConsole);
    }

    public class UserConsole {
        private boolean realConsole;
        private boolean secured;
        private boolean terminated;
        private boolean killed;
        public String inputStr, outputStr;
        private StringBuffer outputBuff = new StringBuffer();
        public final Lock lock = new ReentrantLock();
        public final Condition hasOutput = lock.newCondition();
        public final Condition hasInput = lock.newCondition();
        public UserConsole(boolean realConsole) {
            this.realConsole = realConsole;
            terminated = false;
        }

        public void kill() {
            killed = true;
        }

        public boolean isTerminated() {
            return terminated;
        }

        public boolean isSecured() {
            return secured;
        }

        public void terminate() {
            terminated = true;
            lock.lock();
            try {
                hasOutput.signal();
            } finally {
                lock.unlock();
            }
        }

        public void clearOutput() {
            outputBuff.setLength(0); /* clear */
        }

        public void flushOutput() {
            outputStr = new String(outputBuff);
            lock.lock();
            try {
                hasOutput.signal();
            } finally {
                lock.unlock();
            }
        }
        
        public void print(char ch) {
            if (realConsole)
            {
                System.out.print(ch);
                return;
            }
            outputBuff.append(ch);
        }

        public void printf(String fmt, Object... args) {
            if (realConsole)
            {
                System.out.printf(fmt, args);
                return;
            }
            outputBuff.append(String.format(fmt, args));
        }

        public String readLine(String fmt, Object... args) throws UserException {
            if (realConsole)
                return System.console().readLine(fmt, args);
            outputBuff.append(String.format(fmt, args));
            console.flushOutput();
            lock.lock();
            try {
                hasInput.await();
            } catch (InterruptedException e) {
                System.out.printf("oops%n");
            } finally {
                lock.unlock();
            }
            if (killed)
                throw new UserException(UserException.TypeId.MAINLOOP_QUIT);
            return inputStr;
        }
        public String readPassword(String fmt, Object... args) throws UserException {
            if (realConsole)
                return new String(System.console().readPassword(fmt, args));
            outputBuff.append(String.format(fmt, args));
            console.flushOutput();
            secured = true;
            lock.lock();
            try {
                hasInput.await();
            } catch (InterruptedException e) {
                System.out.printf("oops%n");
            } finally {
                lock.unlock();
            }
            secured = false;
            if (killed)
                throw new UserException(UserException.TypeId.MAINLOOP_QUIT);
            return inputStr;
        }
    }

    private UserConsole console;

    public UserConsole getConsole() {
        return console;
    }

    public void mainLoop() throws InternalError {
        try {
            for (Navigator navi = new MainNavi(this.getConsole(), presenter);;)
            {
                navi.greetingMsg();
                navi.help(new String[]{});
                Navigator next = navi;
                while (next == navi)
                {
                    String raw = getConsole().readLine(navi.getPrompt());
                    if (raw == null)
                        throw new UserException(UserException.TypeId.MAINLOOP_QUIT);
                    String[] tokens = raw.split("\\s+");
                    if (!(tokens.length < 1 || tokens[0].length() == 0))
                    {
                        next = navi.navigate(tokens[0], Arrays.copyOfRange(tokens, 1, tokens.length));
                        if (next == null)
                        {
                            getConsole().printf("Command not found: %s%n", tokens[0]);
                            next = navi;
                        }
                    }
                }
                navi = next;
            }
        } catch (UserException e) {
            switch (e.type)
            {
                case MAINLOOP_QUIT:
                    break;
                default:
                    getConsole().printf("unhandled UserException\n");
            }
        }
        console.terminate();
    }

    static public class CUIThread extends Thread {
        private Connector con = new Connector();
        public CUIView view = new CUIView(con.conn, false);
        public CUIThread() throws InternalError {
        }
        public void run() {
            try {
                view.mainLoop();
                con.closeConnection();
            } catch (InternalError e) {
                e.printStackTrace();
            }
        }
    }
}
