package bookmgr;
import java.sql.*;

public class Connector {
    public Connection conn;
    public Statement stmt;
    public Connector() throws InternalError {
        try
        {
            /*
            String userName = "root";
            String password = "haha";
            String url = "jdbc:mysql://localhost/project";
            */
            String userName = "acmdbu03";
            String password = "cbk7qahf";
            String url = "jdbc:mysql://georgia.eng.utah.edu/acmdb03";

            Class.forName ("com.mysql.jdbc.Driver").newInstance ();
            conn = DriverManager.getConnection(url, userName, password);

            //DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
            //stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            //stmt = conn.createStatement();
            //stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        } 
        catch (Exception e)
        {
            e.printStackTrace();
            throw new InternalError("Unable to open mysql jdbc connection\n");
        }
    }

    public void closeConnection() throws InternalError {
        try
        {
            conn.close();
        }
        catch (Exception e)
        {
            throw new InternalError("error while closing connection");
        }
    }
}
