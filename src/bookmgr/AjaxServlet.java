package bookmgr;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class AjaxServlet extends HttpServlet {

    static final public long serialVersionUID = 1;

    static String toJSON(CUIView.UserConsole console) {
        return String.format("{ \"stdout\": %s, \"secured\": %s }",
                Utils.jsonQuote(console.outputStr),
                console.isSecured());
    }

    static private CUIView.UserConsole firstPrompt(CUIView.CUIThread thread,
                                                    PrintWriter out, HttpSession session) {
        CUIView.UserConsole console = thread.view.getConsole();
        try {
            thread.start();
        } catch (IllegalThreadStateException e) {
            try {
                thread = new CUIView.CUIThread();
                console = thread.view.getConsole();
            } catch (InternalError exc) {
                out.write("internal error");
                return null;
            }
            thread.start();
            session.setAttribute("thread", thread);
        }
        console.lock.lock();
        try {
            console.hasOutput.await();
        } catch (InterruptedException e) {
            System.out.printf("oops%n");
        } finally {
            console.lock.unlock();
        }
        out.write(toJSON(console));
        console.clearOutput();
        return console;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        CUIView.CUIThread thread = (CUIView.CUIThread)session.getAttribute("thread");
        if (thread == null || thread.view == null)
        {
            out.write("internal error\n");
            return;
        }
        CUIView.UserConsole console = thread.view.getConsole();
        if (!thread.isAlive())
        {
            if (firstPrompt(thread, out, session) == null)
                return;
        }
        else
        {
            console.inputStr = request.getParameter("cmd");
            if (console.inputStr == null)
                out.write("invalid input\n");
            else
            {
                console.lock.lock();
                try {
                    console.hasInput.signal();
                    console.hasOutput.await();
                } catch (InterruptedException e) {
                    System.out.printf("oops%n");
                } finally {
                    console.lock.unlock();
                }
                if (console.isTerminated())
                { /* when the user has quitted */
                    session.setAttribute("thread", null);
                    session.invalidate();
                    session = request.getSession();
                    thread = (CUIView.CUIThread)session.getAttribute("thread");
                    console = firstPrompt(thread, out, session);
                }
                else
                {
                    out.write(toJSON(console));
                    console.clearOutput();
                }
            }
        }
    }
}
