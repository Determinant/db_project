package bookmgr;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Installer {
    static private String defaultFileName = "../install.mysql";
    static private String defaultRootPasswd = "123456";

    public static void main(String[] args) {
        try
        {
            Connector con = new Connector();
            Presenter presenter = new Presenter(con.conn);
            try
            {
                BufferedReader buff = new BufferedReader(new FileReader(defaultFileName));
                try
                {
                    StringBuilder sb = new StringBuilder();
                    int ret;
                    while ((ret = buff.read()) != -1)
                    {
                        char ch = (char)ret;
                        sb.append(ch);
                        if (ch == ';')
                        {
                            System.out.printf("%s\n", sb.toString());
                            presenter.executeUpdate(sb.toString());
                            sb.setLength(0); /* clear the buffer */
                        }
                    }
                }
                finally
                {
                    buff.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
                throw new InternalError("error while importing mysql file");
            }
            Presenter.User root = presenter.new User("root");
            root.privilege = Presenter.User.PRIV_ADMIN;
            if (!root.register(defaultRootPasswd))
                throw new InternalError("failed to register root account");
            con.closeConnection();
            System.out.printf("finished\n");
        }
        catch (InternalError e)
        {
            e.printStackTrace();
        }
    }
}
