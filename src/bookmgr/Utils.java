package bookmgr;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.Arrays;

class Utils {

    static private Random random;
    static public void initRandomSeed() {
        random = new Random(System.currentTimeMillis());
    }

    static public byte[] genRandomBinary(int length) {
        byte[] res = new byte[length];
        random.nextBytes(res);
        return res;
    }

    static public byte[] sha256Digest(byte[] str) throws InternalError {
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(str);
            return md.digest();
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new InternalError("error while digesting");
        }
    }

    static public String genSessionToken(String username, String password) throws InternalError {
        byte[] tmp = new byte[username.length() + Presenter.User.SESSION_TOKEN_LENGTH + password.length() + 2];
        ByteBuffer buff = ByteBuffer.wrap(tmp);
        buff.put(username.getBytes());
        buff.put((byte)0);
        buff.put(genRandomBinary(Presenter.User.SESSION_TOKEN_LENGTH));
        buff.put((byte)0);
        buff.put(password.getBytes());
        return showHEX(sha256Digest(tmp));
    }

    static public byte[] genPasswordHash(String username, byte[] salt, String password) throws InternalError {
        byte[] tmp = new byte[username.length() + salt.length + password.length() + 2];
        ByteBuffer buff = ByteBuffer.wrap(tmp);
        buff.put(username.getBytes());
        buff.put((byte)0);
        buff.put(salt);
        buff.put((byte)0);
        buff.put(password.getBytes());
        return sha256Digest(tmp);
    }


    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String showHEX(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    static String charLine(int length, char ch) {
        char[] chars = new char[length];
        Arrays.fill(chars, ch);
        return new String(chars);
    }

    static final int SHOW_HEADER = 1;
    static final int SHOW_EMPTY = 2;
    static void printTabular(CUIView.UserConsole console, String[] heads, int mode,
                            char cChar, char hChar, char vChar, Object... params) {
        int len = heads.length;
        if ((mode & SHOW_HEADER) > 0)
        {
            console.print(cChar);
            for (int i = 0; i < len; i++)
                console.printf("%s%c", charLine(heads[i].length() + 2, hChar), cChar);
            console.printf("%n%c", vChar);
            for (int i = 0; i < len; i++)
                console.printf(" %s %c", heads[i], vChar);
            console.printf("%n");
        }

        console.print(cChar);
        for (int i = 0; i < len; i++)
            console.printf("%s%c", charLine(heads[i].length() + 2, hChar), cChar);
        console.printf("%n");
        int[] colPos = new int[len];
        String[][] colStr = new String[len][];

        for (int i = 0; i < len; i++)
        {
            colPos[i] = 0;
            colStr[i] = (String[])params[i];
        }

        int colNum = colStr[0].length;
        if (colNum == 0)
        { 
            if ((mode & SHOW_EMPTY) > 0)
            {
                final String emptyStr = " empty";
                int fill = 0;
                for (int i = 0; i < len; i++)
                    fill += heads[i].length() + 3;
                fill--;
                console.printf("%c%s%s%c%n%c%s%c%n", vChar, emptyStr,
                                charLine(fill - emptyStr.length(), ' '),
                        vChar,
                        cChar,
                        charLine(fill, hChar),
                        cChar);
            }
            return;
        }
        for (int rowPos = 0; rowPos < colNum; rowPos++)
            for (boolean flag = true;;)
            {
                flag = false;
                String[] buff = new String[len];
                int[] consumed = new int[len];
                for (int i = 0; i < len; i++)
                {
                    consumed[i] = colStr[i][rowPos].length() - colPos[i];
                    if (heads[i].length() < consumed[i])
                        consumed[i] = heads[i].length();
                    if (consumed[i] > 0) flag = true;
                }
                if (!flag)
                {
                    for (int i = 0; i < len; i++)
                        colPos[i] = 0;
                    break;
                }
                console.print(vChar);
                for (int i = 0; i < len; i++)
                {
                    if (consumed[i] > 0)
                    {
                        buff[i] = colStr[i][rowPos].substring(colPos[i], colPos[i] + consumed[i]);
                        colPos[i] += consumed[i];
                    }
                    else
                        buff[i] = "";
                    buff[i] += charLine(heads[i].length() - buff[i].length(), ' ');
                    console.printf(" %s %c", buff[i], vChar);
                }
                console.printf("%n");
            }
        console.print(cChar);
        for (int i = 0; i < len; i++)
            console.printf("%s%c", charLine(heads[i].length() + 2, hChar), cChar);
        console.printf("%n");
    }

    public static String jsonQuote(String string) {
        if (string == null || string.length() == 0) {
            return "\"\"";
        }

        char c = 0;
        int i;
        int len = string.length();
        StringBuilder sb = new StringBuilder(len + 4);
        String t;

        sb.append('"');
        for (i = 0; i < len; i += 1) {
            c = string.charAt(i);
            switch (c) {
                case '\\':
                case '"':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '/':
                    //                if (b == '<') {
                    sb.append('\\');
                    //                }
                    sb.append(c);
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                default:
                    if (c < ' ') {
                        t = "000" + Integer.toHexString(c);
                        sb.append("\\u" + t.substring(t.length() - 4));
                    } else {
                        sb.append(c);
                    }
            }
        }
        sb.append('"');
        return sb.toString();
    }
}
