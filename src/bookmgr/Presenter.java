package bookmgr;
import java.sql.*;
import java.util.LinkedList;

public class Presenter {

    private Connection conn; 

    public PreparedStatement buildStatement(String rawSQL, Object[] params) throws InternalError {
        PreparedStatement psql;
        try 
        {
            psql = conn.prepareStatement(rawSQL, Statement.RETURN_GENERATED_KEYS);
            int nparams = psql.getParameterMetaData().getParameterCount();
            for (int i = 0; i < nparams; i++)
            {
                String typeName = (String)params[i * 2];
                Object p = params[i * 2 + 1];
                if (typeName.equals("int"))
                    psql.setInt(i + 1, (Integer)p);
                else if (typeName.equals("str"))
                    psql.setString(i + 1, (String)p);
                else if (typeName.equals("bin"))
                    psql.setBytes(i + 1, (byte[])p);
                else if (typeName.equals("bool"))
                    psql.setBoolean(i + 1, (Boolean)p);
                else if (typeName.equals("float"))
                    psql.setFloat(i + 1, (Float)p);
                else if (typeName.equals("time"))
                    psql.setTimestamp(i + 1, (Timestamp)p);
                else
                    throw new InternalError("Unrecognized SQL parameter type");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new InternalError("Parameterization failed");
        }
        return psql;
    }

    public ResultSet executeQuery(String rawSQL, Object... params) throws InternalError {
        try
        {
            return buildStatement(rawSQL, params).executeQuery();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new InternalError("error while execute query");
        }
    }

    public ResultSet executeUpdate(String rawSQL, Object... params) throws InternalError {
        try
        {
            PreparedStatement psql = buildStatement(rawSQL, params);
            psql.executeUpdate();
            return psql.getGeneratedKeys();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new InternalError("error while execute update");
        }
    }

    public boolean resultIsNull(ResultSet rs) throws InternalError {
        try
        {
            if (rs.next())
                return false; /* already exists */
        }
        catch (SQLException e)
        {
            throw new InternalError("error while checking result existance");
        }
        return true;
    }

    static String subNull(String str) {
        if (str == null) str = "N/A";
        return str;
    }

    public Presenter(Connection conn) {
        Utils.initRandomSeed();
        this.conn = conn;
    }

    public class haveFeedback {
        final static int MAX_SCORE = 10;
        final static int MAX_REMARK_LENGTH = 4096;
        final static int MAX_USEFULNESS = 2;

        String remark;
        int score;

        String username, fullName, isbn, title;
        Timestamp time;
        float average;

        LinkedList<haveFeedback> usefulFeedback(int limit) throws InternalError {
            try {
                ResultSet rs = executeQuery("select username, full_name, isbn, title, score, remark, ifnull(AVG(rate), 0) as average from " +
                        "haveFeedback as H " + 
                        "inner join Customer as C on C.uid = H.uid " +
                        "inner join Book as B on B.bid = H.bid " +
                        "left outer join rateFeedback R on H.uid = R.fb_uid and H.bid = R.fb_bid " +
                        "group by H.uid, H.bid order by average desc limit ?",
                        "int", limit);
                LinkedList<haveFeedback> fbs = new LinkedList<haveFeedback>();
                while (rs.next())
                {
                    haveFeedback fb = new haveFeedback();
                    fb.username = rs.getString(1);
                    fb.fullName = subNull(rs.getString(2));
                    fb.isbn = rs.getString(3);
                    fb.title = rs.getString(4);
                    fb.score = rs.getInt(5);
                    fb.remark = subNull(rs.getString(6));
                    fb.average = rs.getFloat(7);
                    fbs.add(fb);
                }
                return fbs;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while getting useful feedback");
            }
        }

        void rate(int rater, String username, String isbn, int rate) throws UserException, InternalError {
            try {
                int uid;
                ResultSet rs = executeQuery("select uid from Customer where username = ?",
                        "str", username);
                if (!rs.next()) /* no such user */
                    throw new UserException(UserException.TypeId.USER_NOT_EXIST);
                uid = rs.getInt(1);
                if (uid == rater)
                    throw new UserException(UserException.TypeId.RATE_SELF_FORBIDDEN);
                int bid;
                rs = executeQuery("select bid from Book where isbn = ?",
                        "str", isbn);
                if (!rs.next()) /* no such book */
                    throw new UserException(UserException.TypeId.BOOK_NOT_EXIST);
                bid = rs.getInt(1);
                rs = executeQuery("select uid from haveFeedback where uid = ? and bid = ?",
                        "int", uid,
                        "int", bid);
                if (resultIsNull(rs))
                    throw new UserException(UserException.TypeId.FB_NOT_EXIST);
                rs = executeQuery("select rater from rateFeedback where rater = ? and fb_uid = ? and fb_bid = ?",
                        "int", rater,
                        "int", uid,
                        "int", bid);
                if (!resultIsNull(rs))
                    throw new UserException(UserException.TypeId.RATE_ALREADY_EXIST);
                executeUpdate("insert into rateFeedback (rate, rater, fb_uid, fb_bid) values (?, ?, ?, ?)",
                        "int", rate,
                        "int", rater,
                        "int", uid,
                        "int", bid);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while rating feedback");
            }
        }
            
        void add(int uid, String isbn, java.util.Date date) throws UserException, InternalError {
            try { 
                int bid;
                ResultSet rs = executeQuery("select bid from Book where isbn = ?",
                        "str", isbn);
                if (!rs.next()) /* no such book */
                    throw new UserException(UserException.TypeId.BOOK_NOT_EXIST);
                bid = rs.getInt(1);
                rs = executeQuery("select uid, bid from haveFeedback where uid = ? and bid = ?",
                        "int", uid,
                        "int", bid);
                if (!resultIsNull(rs))
                    throw new UserException(UserException.TypeId.FB_ALREADY_EXIST); /* feedback already exists */

                executeUpdate("insert into haveFeedback (remark, score, fb_time, uid, bid) values (?, ?, ?, ?, ?)",
                        "str", remark,
                        "int", score,
                        "time", new Timestamp(date.getTime()),
                        "int", uid,
                        "int", bid);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while adding feedback");
            }
        }
    }

    public class Book {
        final static int MAX_TITLE_LENGTH = 128;
        final static int MAX_ISBN_LENGTH = 20;
        final static int MAX_SUBJECT_LENGTH = 32;
        final static int MAX_KEYWORDS_LENGTH = 128;
        final static int DEFAULT_BOOK_NUM_OF_COPIES = 1;
        final static int MAX_COPIES = 999;
        final static int MAX_YEAR = 9999;
        final static int DEFAULT_BOOK_YEAR = 1970;
        final static float MAX_PRICE = 9999999.0f;
        final static int DEFAULT_BOOK_HARDCOVER = 1;
        final static int MAX_PUBLISHER_LENGTH = 64;
        final static int MAX_AUTHORS_LENGTH = 1024;
        final static int MAX_AUTHOR_LENGTH = 64;

        int bid, pid;
        String title, publisher, subject, keywords, isbn;
        String[] authors;
        int numOfCopies, yearOfPub;
        float price;
        boolean hardcover;

        public String formattedAuthors() {
            String res = authors[0];
            for (int i = 1; i < authors.length; i++)
                res += ", " + authors[i];
            return res;
        }

        public void getAuthors() throws InternalError {
            try {
                ResultSet rs = executeQuery("select name from " +
                        "Author as A inner join " +
                        "writeBook as w on w.aid = A.aid " + 
                        "where bid = ?",
                        "int", bid);
                LinkedList<String> alist = new LinkedList<String>();
                while (rs.next())
                    alist.add(rs.getString(1));
                authors = alist.toArray(new String[alist.size()]);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while fetching authors");
            }
        }

        LinkedList<haveFeedback> getFeedback() throws InternalError {
            try {
                ResultSet rs = executeQuery("select username, full_name, score, remark, ifnull(AVG(rate), 0), fb_time as average from " +
                        "haveFeedback as H " + 
                        "inner join Customer as C on C.uid = H.uid " +
                        "inner join Book as B on B.bid = H.bid " +
                        "left outer join rateFeedback R on H.uid = R.fb_uid and H.bid = R.fb_bid " +
                        "where H.bid = ? " +
                        "group by H.uid, H.bid order by fb_time desc",
                        "int", bid);
                LinkedList<haveFeedback> fbs = new LinkedList<haveFeedback>();
                while (rs.next())
                {
                    haveFeedback fb = new haveFeedback();
                    fb.username = rs.getString(1);
                    fb.fullName = subNull(rs.getString(2));
                    fb.score = rs.getInt(3);
                    fb.remark = subNull(rs.getString(4));
                    fb.average = rs.getFloat(5);
                    fb.time = rs.getTimestamp(6);
                    fbs.add(fb);
                }
                return fbs;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while getting useful feedback");
            }
        }

        public void order(int uid, int desired, java.util.Date date) throws UserException, InternalError {
            try { 
                ResultSet rs = executeQuery("select bid, num_of_copies from Book where isbn = ?",
                        "str", isbn);
                if (!rs.next())
                    throw new UserException(UserException.TypeId.BOOK_NOT_EXIST); /* book does not exist */
                bid = rs.getInt(1); 
                if ((numOfCopies = rs.getInt(2)) < desired)
                    throw new UserException(UserException.TypeId.BOOK_INSUFFIICENT_COPIES); /* book does not exist */

                executeUpdate("insert into orderBook (num_of_copies, order_time, uid, bid) values (?, ?, ?, ?)",
                        "int", desired,
                        "time", new Timestamp(date.getTime()),
                        "int", uid,
                        "int", bid);
                executeUpdate("update Book set num_of_copies = num_of_copies - ? where bid = ?",
                        "int", desired,
                        "int", bid);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while ordering");
            }
        }

        public boolean morebooks(int num) throws InternalError {
            try { 
                ResultSet rs = executeQuery("select bid, num_of_copies from Book where isbn = ?",
                        "str", isbn);
                if (!rs.next())
                    return false;
                bid = rs.getInt(1); 
                executeUpdate("update Book set num_of_copies = num_of_copies + ? where bid = ?",
                        "int", num,
                        "int", bid);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while ordering");
            }
            return true;
        }

        public boolean newbook() throws InternalError {
            /* TODO: lock the table & use transaction */
            try { 
                ResultSet rs = executeQuery("select bid from Book where isbn = ?",
                        "str", isbn);
                if (!resultIsNull(rs)) return false; /* book already exists */
                rs = executeQuery("select pid from Publisher where name = ?",
                        "str", publisher);
                /* add publisher if not exists */
                if (resultIsNull(rs))
                {
                    rs = executeUpdate("insert into Publisher (name) values (?)",
                            "str", publisher);
                    if (!rs.next()) return false;
                }
                pid = rs.getInt(1);

                rs = executeUpdate("insert into Book (isbn, price, hardcover, title, subject, year_of_pub, num_of_copies, keywords, pid) values (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        "str", isbn,
                        "float", price,
                        "bool", hardcover,
                        "str", title,
                        "str", subject,
                        "int", yearOfPub,
                        "int", numOfCopies,
                        "str", keywords,
                        "int", pid);
                if (!rs.next()) return false;
                bid = rs.getInt(1);
                for (int i = 0; i < authors.length; i++)
                {
                    rs = executeQuery("select aid from Author where name = ?",
                            "str", authors[i]);
                    int aid;
                    /* add author if not exists */
                    if (resultIsNull(rs))
                    {
                        rs = executeUpdate("insert into Author (name) values (?)",
                                "str", authors[i]);
                        if (!rs.next()) return false;
                    }
                    aid = rs.getInt(1);
                    executeUpdate("insert into writeBook (aid, bid) values (?, ?)",
                            "int", aid, "int", bid);

                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while checking publisher");
            }

            return true;
        }
    }

    static public enum OrderMode{
        BY_YEAR,
        BY_AVG_SCORE,
        BY_AVG_TRUSTED_SCORE
    };

    public class User {
        public int uid;
        public String sessionToken;
        public String username;
        public String fullName;
        public String phoneNumber;
        public String address;
        public int privilege = PRIV_NORMAL_USER;

        final static public int SALT_LENGTH = 32;
        final static public int SESSION_TOKEN_LENGTH = 32;
        final static public int MAX_USERNAME_LENGTH = 32;
        final static public int MAX_FULLNAME_LENGTH = 64;
        final static public int MAX_PHONE_NUMBER_LENGTH = 32;
        final static public int MAX_ADDRESS_LENGTH = 64;
        final static public int PRIV_NORMAL_USER = 0;
        final static public int PRIV_ADMIN = 1;

        /* password should not reside in memory for a long period */
        public User(String username) {
            this.username = username;
        }

        LinkedList<User> getTrustedUsers(int limit) throws InternalError {
            try {
                ResultSet rs = executeQuery("select H0.username, H0.full_name, (H0.count + ifnull(H1.count, 0) - ifnull(H2.count, 0)) as val from " +
                        "(select username, full_name, uid, 0 as count from Customer) as H0 " +
                        "left outer join " +
                        "(select h.uid2 as uid, count(*) as count " +
                        " from havePreference as h " +
                        " where h.trust = true group by h.uid2) as H1 on H0.uid = H1.uid " +
                        "left outer join " +
                        "(select h.uid2 as uid, count(*) as count " +
                        " from havePreference as h " +
                        " where h.trust = false group by h.uid2) as H2 on H0.uid = H2.uid " +
                        "order by val desc limit ?",
                        "int", limit);
                LinkedList<User> users = new LinkedList<User>();
                while (rs.next())
                {
                    User u = new User(rs.getString(1));
                    u.fullName = subNull(rs.getString(2));
                    u.uid = rs.getInt(3);
                    users.add(u);
                }
                return users;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while getting most trusted users");
            }
        }

        LinkedList<User> getUsefulUsers(int limit) throws InternalError {
            try {
                ResultSet rs = executeQuery("select C.username, C.full_name, C.uid, ifnull(AVG(r.rate), 0) as average " +
                        "from haveFeedback as h " +
                        "inner join rateFeedback as r on r.fb_bid = h.bid and r.fb_uid = h.uid " +
                        "right outer join Customer as C on r.fb_uid = C.uid " +
                        "group by C.uid " +
                        "order by average desc limit ?",
                        "int", limit);
                LinkedList<User> users = new LinkedList<User>();
                while (rs.next())
                {
                    User u = new User(rs.getString(1));
                    u.fullName = subNull(rs.getString(2));
                    u.uid = rs.getInt(3);
                    users.add(u);
                }
                return users;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while getting most useful users");
            }
        }

        LinkedList<Book> getPopularBooks(int year, int startMonth, int endMonth, int limit) throws InternalError {
            try {
                ResultSet rs = executeQuery("select B.bid, B.isbn, B.title, P.name, ifnull(SUM(O.num_of_copies), 0) as sum from Book as B " +
                        "inner join Publisher as P on P.pid = B.pid " +
                        "left outer join orderBook as O on O.bid = B.bid " +
                        "and YEAR(O.order_time) = ? and " +
                        "? <= MONTH(O.order_time) and MONTH(O.order_time) <= ? " +
                        "group by B.bid, B.isbn, B.title " +
                        "order by sum desc limit ? ",
                        "int", year,
                        "int", startMonth,
                        "int", endMonth,
                        "int", limit);
                LinkedList<Book> books = new LinkedList<Book>();
                while (rs.next())
                {
                    Book b = new Book();
                    b.bid = rs.getInt(1);
                    b.isbn = rs.getString(2);
                    b.title = rs.getString(3);
                    b.publisher = rs.getString(4);
                    b.numOfCopies = rs.getInt(5);
                    b.getAuthors();
                    books.add(b);
                }
                return books;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while getting popular books");
            }
        }

        LinkedList<String> getPopularAuthors(int year, int startMonth, int endMonth, int limit) throws InternalError {
            try {
                ResultSet rs = executeQuery("select A.name, ifnull(SUM(O.num_of_copies), 0) as sum from Author as A " +
                        "inner join writeBook as w on w.aid = A.aid " +
                        "left outer join orderBook as O on O.bid = w.bid " +
                        "and YEAR(O.order_time) = ? and " +
                        "? <= MONTH(O.order_time) and MONTH(O.order_time) <= ? " +
                        "group by A.aid, A.name " +
                        "order by sum desc limit ? ",
                        "int", year,
                        "int", startMonth,
                        "int", endMonth,
                        "int", limit);
                LinkedList<String> authors = new LinkedList<String>();
                while (rs.next())
                    authors.add(rs.getString(1));
                return authors;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while getting popular books");
            }
        }

        LinkedList<String> getPopularPublishers(int year, int startMonth, int endMonth, int limit) throws InternalError {
            try {
                ResultSet rs = executeQuery("select P.name, ifnull(SUM(O.num_of_copies), 0) as sum from Publisher as P " +
                        "inner join Book as B on B.pid = P.pid " +
                        "left outer join orderBook as O on O.bid = B.bid " +
                        "and YEAR(O.order_time) = ? and " +
                        "? <= MONTH(O.order_time) and MONTH(O.order_time) <= ? " +
                        "group by P.pid, P.name " +
                        "order by sum desc limit ?",
                        "int", year,
                        "int", startMonth,
                        "int", endMonth,
                        "int", limit);
                LinkedList<String> pubs = new LinkedList<String>();
                while (rs.next())
                    pubs.add(rs.getString(1));
                return pubs;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while getting popular books");
            }
        }

        boolean checkOneDegSep(String author1, String author2) throws InternalError {
            ResultSet rs = executeQuery("select A2.aid from " +
                    "(select B.bid from Book as B " +
                    "inner join writeBook as w on B.bid = w.bid " +
                    "inner join Author as A on A.aid = w.aid " +
                    "where A.name = ?) as B2 " +
                    "inner join writeBook as w2 on B2.bid = w2.bid " +
                    "inner join Author as A2 on A2.aid = w2.aid " +
                    "where A2.name = ?",
                    "str", author1,
                    "str", author2);
            return !resultIsNull(rs);
        }

        boolean checkTwoDegSep(String author1, String author2) throws InternalError {
            ResultSet rs = executeQuery("select A4.aid from " +
                    "(select B3.bid from Book as B3 " +
                    " inner join writeBook as w3 on B3.bid = w3.bid " +
                    " inner join " +
                    " (select A2.aid from " +
                    "  (select B.bid from Book as B " +
                    "   inner join writeBook as w on B.bid = w.bid " +
                    "   inner join Author as A on A.aid = w.aid " +
                    "   where A.name = ?) as B2 " +
                    "  inner join writeBook as w2 on B2.bid = w2.bid " +
                    "  inner join Author as A2 on A2.aid = w2.aid) as A3 on A3.aid = w3.aid) as B4 " +
                    "inner join writeBook as w4 on B4.bid = w4.bid " +
                    "inner join Author as A4 on A4.aid = w4.aid " +
                    "where A4.name = ?; ",
                    "str", author1,
                    "str", author2);
            return !resultIsNull(rs);
        }

        public LinkedList<Book> recommend(int bid) throws InternalError {
            try {
                ResultSet rs = executeQuery("select B.bid, B.isbn, B.title, P.name, SUM(O.num_of_copies) from " +
                        "(select O1.bid, O1.num_of_copies from orderBook as O1, " +
                            "(select O2.uid from orderBook as O2 where O2.bid = ? and O2.uid <> ?) as U " +
                            "where O1.uid = U.uid and O1.bid <> ?) as O " +
                        "inner join Book as B on O.bid = B.bid " +
                        "inner join Publisher as P on B.pid = P.pid " +
                        "group by B.bid, B.isbn, B.title, P.name " +
                        "order by SUM(O.num_of_copies) desc",
                        "int", bid,
                        "int", uid,
                        "int", bid);
                LinkedList<Book> books = new LinkedList<Book>();
                while (rs.next())
                {
                    Book b = new Book();
                    b.bid = rs.getInt(1);
                    b.isbn = rs.getString(2);
                    b.title = rs.getString(3);
                    b.publisher = rs.getString(4);
                    b.getAuthors();
                    books.add(b);
                }
                return books;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while proceeding recommendation");
            }
        }

        public void removePref(String username) throws UserException, InternalError {
             try {
                int uid2;
                ResultSet rs = executeQuery("select uid from Customer where username = ?",
                        "str", username);
                if (!rs.next()) /* no such user */
                    throw new UserException(UserException.TypeId.USER_NOT_EXIST);
                uid2 = rs.getInt(1);
                rs = executeQuery("select trust from havePreference where uid1 = ? and uid2 = ?",
                        "int", uid,
                        "int", uid2);
                if (resultIsNull(rs))
                    throw new UserException(UserException.TypeId.TRUST_NOT_EXIST);
                else
                {
                    executeUpdate("delete from havePreference where uid1 = ? and uid2 = ?",
                            "int", uid,
                            "int", uid2);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while removing preference");
            }
        }


        public void addPref(String username, boolean trust) throws UserException, InternalError {
             try {
                int uid2;
                ResultSet rs = executeQuery("select uid from Customer where username = ?",
                        "str", username);
                if (!rs.next()) /* no such user */
                    throw new UserException(UserException.TypeId.USER_NOT_EXIST);
                uid2 = rs.getInt(1);
                rs = executeQuery("select trust from havePreference where uid1 = ? and uid2 = ?",
                        "int", uid,
                        "int", uid2);
                if (resultIsNull(rs))
                {
                    executeUpdate("insert into havePreference (trust, uid1, uid2) values (?, ?, ?)",
                            "bool", trust,
                            "int", uid,
                            "int", uid2);
                }
                else
                {
                    executeUpdate("update havePreference set trust = ? where uid1 = ? and uid2 = ?",
                            "bool", trust,
                            "int", uid,
                            "int", uid2);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while giving preference");
            }
        }

        public boolean login(String password) throws InternalError {
            ResultSet rs;
            byte[] salt;
            try
            {
                rs = executeQuery("select salt from Customer where username = ?",
                        "str", username);
                if (!rs.next())
                    return false; /* incorrect username */
                salt = rs.getBytes(1);
                if (salt.length != SALT_LENGTH)
                    throw new InternalError("inconsistent salt length");
                rs = executeQuery("select uid, full_name, phone_number, address, privilege from Customer where username = ? and passwd_hash = ?",
                        "str", username,
                        "bin", Utils.genPasswordHash(username, salt, password));
                /*
                System.out.printf("salt: %s\n", Utils.showHEX(salt));
                System.out.printf("password hash: %s\n", Utils.showHEX(Utils.genPasswordHash(username, salt, password)));
                */
                if (!rs.next())
                    return false; /* incorrect password */
                uid = rs.getInt(1);
                fullName = subNull(rs.getString(2));
                phoneNumber = subNull(rs.getString(3));
                address = subNull(rs.getString(4));
                privilege = rs.getInt(5);
                /* regenerate session_token to invalidate previous session */
                sessionToken = Utils.genSessionToken(username, password);
                /* update session_token */
                executeUpdate("update Customer set session_token = ? where uid = ?",
                        "str", sessionToken,
                        "int", uid);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                throw new InternalError("error while parsing login result");
            }
            return true;
        }

        public void logout() throws InternalError {
            /* generate junk token to invalidate the original active session */
            sessionToken = Utils.genSessionToken("", "");
            executeUpdate("update Customer set session_token = ? where uid = ?",
                    "str", sessionToken,
                    "int", uid);
        }


        public boolean register(String password) throws InternalError {
            assert sessionToken == null;
            byte[] salt = Utils.genRandomBinary(SALT_LENGTH);
            byte[] passwd_hash = Utils.genPasswordHash(username, salt, password);
            /*
            System.out.printf("salt: %s\n", Utils.showHEX(salt));
            System.out.printf("password hash: %s\n", Utils.showHEX(Utils.genPasswordHash(username, salt, password)));
            */

            /* TODO: lock the table */
            ResultSet rs = executeQuery("select uid from Customer where username = ?",
                                        "str", username);
            if (!resultIsNull(rs)) return false; /* user already exists */
            executeUpdate("insert into Customer (full_name, username, passwd_hash, salt, phone_number, address, privilege) values (?, ?, ?, ?, ?, ?, ?)",
                    "str", fullName,
                    "str", username,
                    "bin", passwd_hash,
                    "bin", salt,
                    "str", phoneNumber,
                    "str", address,
                    "int", privilege);
            return true;
        }


        public LinkedList<Book> findBooks(String author, String publisher, String title, String subject, boolean conj1, boolean conj2, boolean conj3, OrderMode mode) throws InternalError {
            try {
                ResultSet rs = null;
                author = "%" + author + "%";
                publisher = "%" + publisher + "%";
                title = "%" + title + "%";
                subject = "%" + subject + "%";
                String sql = String.format("(select B.bid, B.isbn, B.title, P.name as pub_name, B.year_of_pub, B.price, B.hardcover, B.subject, B.num_of_copies, B.keywords from Book as B " +
                    " inner join writeBook as w on w.bid = B.bid " +
                    " inner join Author as A on w.aid = A.aid " +
                    " inner join Publisher as P on P.pid = B.pid " +
                    " where A.name like ? %s P.name like ? %s B.title like ? %s B.subject like ? " +
                    " group by B.bid)", conj1 ? "and" : "or", conj2 ? "and" : "or", conj3 ? "and" : "or");
                String prelude = "select B0.bid, B0.isbn, B0.title, B0.pub_name, B0.year_of_pub, B0.price, B0.hardcover, B0.subject, B0.num_of_copies, B0.keywords";
                switch (mode)
                {
                    case BY_YEAR:
                        sql = String.format("%s from %s as B0 order by B0.year_of_pub", prelude, sql);
                        rs = executeQuery(sql, "str", author, "str", publisher, "str", title, "str", subject);
                        break;
                    case BY_AVG_SCORE:
                        sql = String.format("%s from %s as B0 " +
                                            "left outer join haveFeedback as h on h.bid = B0.bid " +
                                            "group by B0.bid, B0.isbn, B0.title, B0.pub_name " +
                                            "order by ifnull(AVG(h.score), 0) desc", prelude, sql);
                        rs = executeQuery(sql, "str", author, "str", publisher, "str", title, "str", subject);
                        break;
                    case BY_AVG_TRUSTED_SCORE:
                        sql = String.format(
                                            "%s from haveFeedback as h " +
                                            "inner join havePreference as h2 on h2.trust = true and h2.uid1 = ? and h2.uid2 = h.uid " +
                                            "right outer join %s as B0 on h.bid = B0.bid " +
                                            "group by B0.bid, B0.isbn, B0.title, B0.pub_name " +
                                            "order by ifnull(AVG(h.score), 0) desc", prelude, sql);
                        rs = executeQuery(sql, "int", uid,
                                            "str", author, "str", publisher, "str", title, "str", subject);
                        break;
                    default: assert false;
                }
                LinkedList<Book> books = new LinkedList<Book>();
                while (rs.next())
                {
                    Book b = new Book();
                    b.bid = rs.getInt(1);
                    b.isbn = rs.getString(2);
                    b.title = rs.getString(3);
                    b.publisher = rs.getString(4);
                    b.yearOfPub = rs.getInt(5);
                    b.price = rs.getFloat(6);
                    b.hardcover = rs.getBoolean(7);
                    b.subject = rs.getString(8);
                    b.numOfCopies = rs.getInt(9);
                    b.keywords = rs.getString(10);
                    b.getAuthors();
                    books.add(b);
                }
                return books;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while finding books");
            }
        }

        public Book getBook(String isbn) throws UserException, InternalError {
            try {
                ResultSet rs = null;
                rs = executeQuery("select B.bid, B.isbn, B.title, P.name, B.year_of_pub, B.price, B.hardcover, B.subject, B.num_of_copies, B.keywords " +
                        "from Book as B " +
                        "inner join Publisher as P on P.pid = B.pid " +
                        "where B.isbn = ?",
                        "str", isbn);
                if (!rs.next())
                    throw new UserException(UserException.TypeId.BOOK_NOT_EXIST);
                Book b = new Book();
                b.bid = rs.getInt(1);
                b.isbn = rs.getString(2);
                b.title = rs.getString(3);
                b.publisher = rs.getString(4);
                b.yearOfPub = rs.getInt(5);
                b.price = rs.getFloat(6);
                b.hardcover = rs.getBoolean(7);
                b.subject = rs.getString(8);
                b.numOfCopies = rs.getInt(9);
                b.keywords = rs.getString(10);
                b.getAuthors();
                return b;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new InternalError("error while finding books");
            }
        }

    }
}
