package bookmgr;
public class PingPongTester {

    public static void main(String[] args) {
        try {
            final Connector con = new Connector();
            final CUIView cui = new CUIView(con.conn, false);
            Runnable mainThread = new Runnable() {
                public void run() {
                    try {
                        cui.mainLoop();
                        con.closeConnection();
                    } catch (InternalError e) {
                        e.printStackTrace();
                    }
                }
            };

            CUIView.UserConsole console = cui.getConsole();
            (new Thread(mainThread)).start();
            /* get the first prompt */
            console.lock.lock();
            try {
                console.hasOutput.await();
            } catch (InterruptedException e) {
                System.out.printf("oops%n");
            } finally {
                console.lock.unlock();
            }
            System.out.printf(console.outputStr);
            console.clearOutput();
            while (!console.isTerminated())
            {
                console.inputStr = System.console().readLine();
                console.lock.lock();
                try {
                    console.hasInput.signal();
                    console.hasOutput.await();
                } catch (InterruptedException e) {
                    System.out.printf("oops%n");
                } finally {
                    console.lock.unlock();
                }
                System.out.printf(console.outputStr);
                console.clearOutput();
            }
        } catch (InternalError e) {
            e.printStackTrace();
        }
    }
}

