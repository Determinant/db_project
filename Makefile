build:
	mkdir -p bin
	javac src/bookmgr/*.java -d bin
test:
	cd bin; java -cp '../mysql.jar:.' bookmgr.PingPongTester
run:
	cd bin; java -cp '../mysql.jar:.' bookmgr.Main
install:
	cd bin; java -cp '../mysql.jar:.' bookmgr.Installer
clean:
	-rm -r bin
deploy:
	rsync -avP bin/ /var/lib/tomcat-7-testbed/webapps/ROOT/WEB-INF/classes/
